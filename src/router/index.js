import Vue from "vue";
import VueRouter from "vue-router";
import garena from "@/components/applications/garena";
import vat from "@/components/applications/vat";
import multitable from "@/components/applications/multitable";
import ptg from "@/components/applications/ptg";
import vsport from "@/components/applications/vsport";










Vue.use(VueRouter);

const routes = [
  
  
  {
    path: "/",
    name: "garena",
    component: garena,
  },
  {
    path: "/vat",
    name: "vat",
    component: vat,
  },
  {
    path: "/multitable",
    name: "multitable",
    component: multitable,
  },
  {
    path: "/ptg",
    name: "ptg",
    component: ptg,
  },
  {
    path: "/vsport",
    name: "vsport",
    component: vsport,
  },

  
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
